/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/


	
	
	//first function here:
function fullName(){
	let firstname = prompt('Enter you first name: ')
	let lastName = prompt('Enter you last name: ')
	let address =prompt('Enter you address: ')
	var age = prompt('Enter you Age: ')
	console.log('Please Enter you full name' +firstname ," " +lastName);
	console.log('Your address: ' +address);
	console.log('Your age: ' +age);


	
	
};
fullName();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

let globalName = 'My Favoite bands';

	function favBands(){

		let bands = ' 1.A1\n 2.parokya \n 3.shakol\n 4.RocksTeddy\n 5.Spongecola' ;
		console.log('My Favorite Bands:\n' +bands);
	
	}
favBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

let topMovies= 'My Top 5 Movies: ';
	function movies(){
		let movieOne = 'Top gun';
		let faveOne = ' Rotten Tomato Ratings: 83%'
		console.log('1. \n'+movieOne+faveOne);

		let movieTwo = 'Top Gun Maverick';
		let faveTwo = ' Rotten Tomatoes Rating: 96%'
		console.log('2. \n'+movieTwo+faveTwo);

		let movieThree = 'The fast and the Furious';
		let faveThree = ' Rotten Tomatoes Rating: 76%'
		console.log('3. \n'+movieThree+faveThree);

		let movieFour = 'Men in Black';
		let faveFour= ' Rotten Tomatoes Rating: 70%'
		console.log('4. \n'+movieTwo+faveFour);

		let movieFive = 'Game of Thrones';
		let faveFive = ' Rotten Tomatoes Rating: 85%'
		console.log('5. \n'+movieFive+faveFive);
	}
movies();
 
 /*printUsers();
let printFriends() = UNECESSARY*/
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
/*
console.log(friend1); 
	console.log(friend2); -unecessary*/

printUsers();